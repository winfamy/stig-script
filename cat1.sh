# cat1.sh
# author: 2d Lt Grady Phillips
# this file runs a script to fix STIG settings on RHEL v9


## RHEL-08-010020
# check fips mode
fips-mode-setup --check

# check kernel boot parameter
grub2-editenv list | grep fips

# check if system in FIPS mode after 
cat /proc/sys/crypto/fips_enabled

## RHEL-08-010140
## RHEL 8 operating systems booted with United Extensible Firmware Interface (UEFI) must require authentication upon booting into single-user mode and maintenance.
# check for "grub.pbkdf2.sha512" to make sure password is sufficiently hashed
grep -iw grub2_password /boot/efi/EFI/redhat/user.cfg

# generate HASHED grub2 password for grub superusers
grub2-setpassword

## RHEL-08-010150
## RHEL 8 operating systems booted with a BIOS must require authentication upon booting into single-user and maintenance modes.
# check to see if encrypted password for BIOS boots
grep -iw grub2_password /boot/grub2/user.cfg

# set grub2 superuser password
grub2-setpassword

## RHEL-08-010370
## RHEL 8 must prevent the installation of software, patches, service packs, device drivers, or operating system components from a repository without verification they have been digitally signed using a certificate that is issued by a Certificate Authority (CA) that is recognized and approved by the organization.
# check that YUM verifies the signature of packages from repos
grep -E '^\[.*\]|gpgcheck' /etc/yum.repos.d/*.repo

# NOTE: CHECK VALID FORMAT FOR GPGCHECK

## RHEL-08-010371
## RHEL 8 must prevent the installation of software, patches, service packs, device drivers, or operating system components of local packages without verification they have been digitally signed using a certificate that is issued by a Certificate Authority (CA) that is recognized and approved by the organization.
# check to see if localpkg_gpgcheck is 1 True or yes
grep -i localpkg_gpgcheck /etc/dnf/dnf.conf

# FIX: set "localpkg_gpgcheck" to True in /etc/dnf/dnf.conf
echo "localpkg_gpgcheck=True" >> /etc/dnf/dnf.conf

## RHEL-08-010460
## There must be no shosts.equiv files on the RHEL 8 operating system.
# find any shosts.equiv files
find / -name shosts.equiv

# remove any shosts.equiv files found
find / -name shosts.equiv -exec rm -f {} \;

## RHEL-08-010470
## There must be no .shosts files on the RHEL 8 operating system.
# find ".shosts" files
find / -name '*.shosts'

# remove any '*.shosts' files found 
find / -name '*.shosts' -exec rm -f {} \;

## RHEL-08-010820
## Unattended or automatic logon via the RHEL 8 graphical user interface must not be allowed.
# check for the value of the AutomaticLoginEnable in the /etc/gdm/custom.conf file
grep -i automaticloginenable /etc/gdm/custom.conf

# add or edit the line for the automaticloginenable in the [daemon] section of the /etc/gdm/custom.conf file
# no command for this

## RHEL-08-020330
## RHEL 8 must not allow accounts configured with blank or null passwords.
# check for permitemptypasswords 
grep -ir permitemptypasswords /etc/ssh/sshd_config*

# substitute permitemptypasswords yes with no
sed -i "s/PermitEmptyPasswords[ \t]*yes/PermitEmptyPasswords no/gI" sshd_config_test

## RHEL-08-040000
## RHEL 8 must not have the telnet-server package installed.
# check if telnet-server installed
yum list installed telnet-server
dnf list installed telnet-server

# uninstall telnet-server
yum remove telnet-server
dnf remove telnet-server

## RHEL-08-040010
## RHEL 8 must not have the rsh-server package installed
# check to see if rsh-server installed
yum list installed rsh-server
dnf list installed rsh-server

# remove rsh-server
yum remove rsh-server
dnf remove rsh-server

## RHEL-08-040170
## The x86 Ctrl-Alt-Delete key sequence must be disabled on RHEL 8.
# check to see if ctrl-alt-del is loaded
systemctl status ctrl-alt-del.target

# disable ctrl-alt-del sequence
systemctl disable ctrl-alt-del.target
systemctl mask ctrl-alt-del.target

## RHEL-08-040171
## The x86 Ctrl-Alt-Delete key sequence in RHEL 8 must be disabled if a graphical user interface is installed.
# check to see if CAD sequence enabled
# if no line result, it is also a finding
grep logout /etc/dconf/db/local.d/*

# add the file to disable the CAD sequence
cat >> /etc/dconf/db/local.d/00-disable-CAD << EOF
[org/gnome/settings-daemon/plugins/media-keys]
logout=''
EOF
# update dconf
dconf update

## RHEL-08-040172
## The systemd Ctrl-Alt-Delete burst key sequence in RHEL 8 must be disabled.
# verify RHEL is not configured to reboot when CAD pressed 7 times within 2 seconds
grep -i ctrlaltdelburstaction /etc/systemd/system.conf

# add the line in /etc/systemd/system.conf
sed -i 's/^.*CtrlAltDelBurstAction=.*/CtrlAltDelBurstAction=none/gI' /etc/systemd/system.conf

## RHEL-08-040190
## The Trivial File Transfer Protocol (TFTP) server package must not be installed if not required for RHEL 8 operational support.
# check if TFTP is installed
yum list installed tftp-server
dnf list installed tftp-server

# remove tftp if installed
yum remove tftp-server
dnf remove tftp-server

## RHEL-08-040200
## The root account must be the only account having unrestricted access to the RHEL 8 system.
# check if anything other accounts are assigned UID 0 in /etc/passwd
awk -F: '$3 === 0 {print $1}' /etc/passwd

## RHEL-08-040360
## A File Transfer Protocol (FTP) server package must not be installed unless mission essential on RHEL 8.
# check for installed ftp servers
yum list installed *ftpd*
dnf list installed *ftpd*

# remove installed ftp server
yum remove vsftpd
dnf remove vsftpd

## RHEL-08-020331
## RHEL 8 must not allow blank or null passwords in the system-auth file.
# verify that null passwords cannot be used
grep -i nullok /etc/pam.d/system-auth

# remove any instances of the "nullok" option in /etc/pam.d/system-auth

## RHEL-08-020332
## RHEL 8 must not allow blank or null passwords in the password-auth file.
# verify that null passwords cannot be used
grep -i nullok /etc/pam.d/password-auth

## RHEL-08-010121
## The RHEL 8 operating system must not have accounts configured with blank or null passwords.
# check the /etc/shadow file for blank passwords with the following command
awk -F: '!$2 { print $1 }' /etc/shadow

# perform password reset or lock account
# passwd username
# password -l username
